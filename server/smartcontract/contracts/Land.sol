// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import "./ERC721.sol";

contract Land is ERC721 {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    struct Parcelle {
        uint256 tokenId;
        string tokenURI;
    }

    mapping(string => bool) _tokenExists;

    // mapping pour stocker tous les actifs disponibles
    mapping(uint256 => Parcelle) public _allTokens;

    constructor() public ERC721("BeninLand", "BENLAND") {
        // _setBaseURI("http://localhost:3000/");
    }

    function safeMint(address _to, string memory _tokenURI)
        public
        returns (uint256)
    {
        require(_to != msg.sender, "_to must be different of msg.sender");
        require(!_tokenExists[_tokenURI], "Require unique token");

        _tokenIds.increment();
        uint256 id = _tokenIds.current();

        _allTokens[id] = Parcelle(id, _tokenURI);
        _safeMint(_to, id);
        _setTokenURI(id, _tokenURI);

        _tokenExists[_tokenURI] = true;

        return id;
    }

    function safeTransfer(
        address _from,
        address _to,
        uint256 _tokenId
    ) public {
        require(_to != msg.sender, "_to must be different of msg.sender");
        require(
            _isApprovedOrOwner(_from, _tokenId),
            "ERC721: transfer caller is not owner nor approved"
        );
        _safeTransfer(_from, _to, _tokenId, "");
    }

    function tokenExist(uint256 tokenId) public view returns (bool) {
        return _exists(tokenId);
    }

    function resetToken(
        uint256 _tokenId,
        address _to,
        string memory _tokenURI
    ) public {
        delete _allTokens[_tokenId];
        _tokenExists[tokenURI(_tokenId)] = false;
        _tokenIds.decrement();
        _burn(_tokenId);
        safeMint(_to, _tokenURI);
    }

    // function getAllParcelle() public view returns(Parcelle[]) {
    //     Parcelle[] mesParcelles = new Parcelle[](_tokenIds);
    //     for (var index = 0; index <= _tokenIds; index++) {
    //         mesParcelles[index] = _allTokens[index];                        
    //     }
    //     return mesParcelles;
    // }
}
