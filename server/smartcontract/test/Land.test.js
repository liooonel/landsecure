const { assert } = require('chai')

const Land = artifacts.require('Land')

require('chai')
    .use(require('chai-as-promised'))
    .should()

contract('Land', (accounts) => {
    let contract

    before(async () => {
        contract = await Land.deployed()
    })

    describe('deployment', async () => {
        it('deploys successfully', async () => {
            const address = contract.address
            assert.notEqual(address, 0x0)
            assert.notEqual(address, '')
            assert.notEqual(address, null)
            assert.notEqual(address, undefined)
        })

        it('has a name', async () => {
            const name = await contract.name()
            assert.equal(name, 'BeninLand', 'name is correct')
        })

        it('has a symbol', async () => {
            const symbol = await contract.symbol()
            assert.equal(symbol, 'BENLAND', 'symbol is correct')
        })

        // it('has a baseURI', async () => {
        //     const baseURI = await contract.baseURI()
        //     assert.equal(baseURI, 'http://localhost:3000/', 'baseuURI  is correct')
        // })

    })

    describe('minting', async () => {
        it('create a new token', async () => {
            const result = await contract.safeMint(accounts[1], "item-id-8u5h2m.json")
            const event = result.logs[0].args
            const totalSupply = await contract.totalSupply()
            const balanceOfAccount1 = await contract.balanceOf(accounts[1])
            //SUCCESS
            assert.equal(totalSupply, 1, 'total amount of tokens is correct')
            assert.equal(balanceOfAccount1, 1, 'balance of account 1 is correct')
            assert.equal(event.tokenId.toNumber(), 1, 'id is correct')
            assert.equal(event.from, '0x0000000000000000000000000000000000000000', 'from is correct')
            assert.equal(event.to, accounts[1], 'to is correct')
            //FAILURE: cannot mint same land twice 
            await contract.safeMint(accounts[1], "item-id-8u5h2m.json").should.be.rejected;
        })
    })

    describe('indexing', async () => {
        it('list lands', async () => {
            await contract.safeMint(accounts[2], "item-id-ko4b27.json")
            await contract.safeMint(accounts[2], "item-id-vdpo5l.json")
            await contract.safeMint(accounts[2], "item-id-sdg3b7.json")

            // const tokenURI = await contract.tokenURI(4)
            // assert.equal(tokenURI, 'http://localhost/item-id-sdg3b7.json', 'tokenURI is correct')

            const totalSupply = await contract.totalSupply()
            assert.equal(totalSupply, 4, 'total amount of tokens is correct')

            const balanceOfAccount2 = await contract.balanceOf(accounts[2])
            assert.equal(balanceOfAccount2, 3, 'balance of account 2 is correct')

            const ownerOf = await contract.ownerOf(2)
            assert.equal(ownerOf, accounts[2], 'account 2 is owner of tokenId 2')

            const tokenId = await contract.tokenOfOwnerByIndex(accounts[2], 2)
            assert.equal(tokenId, 4, 'tokenId is correct')

            let land
            let result = []
            let expected = ['item-id-8u5h2m.json', 'item-id-ko4b27.json', 'item-id-vdpo5l.json', 'item-id-sdg3b7.json']
            for (let i = 1; i <= totalSupply; i++) {
                land = await contract._allTokens(i)
                result.push(land[1]) //Object.entries(land) Object.keys(land) Object.values(land)
            }
            expect(expected).to.eql(result, 'expected my \'expected\' and my \'result\' are same')
        })

        it('tokenId by address', async () => {
            const balance = await contract.balanceOf(accounts[2])
            let ownerTokenList = []
            for (let i = 0; i < balance; i++) {
                const tokenId = await contract.tokenOfOwnerByIndex(accounts[2], i)
                ownerTokenList.push(tokenId.toNumber())
            }
            let expectedList = [2, 3, 4]
            expect(expectedList).to.eql(ownerTokenList, 'expected my \'expected\' and my \'result\' are same')

        })

        it('token exist', async () => {
            const exist = await contract.tokenExist(2)
            assert.equal(exist, 1, 'token exist')
            const exist1 = await contract.tokenExist(5)
            assert.equal(exist1, 0, 'token doesn\'t exist')
        })


        it('reset tokens', async () => {
            const rr = await contract.resetToken(4, accounts[2], "item-id-XXXXXXXX.json")
            // console.log(rr)

            const totalSupply = await contract.totalSupply()
            assert.equal(totalSupply, 4, 'total amount of tokens is correct')

            const balanceOfAccount2 = await contract.balanceOf(accounts[2])
            assert.equal(balanceOfAccount2, 3, 'balance of account 2 is correct')

            let land
            let result = []
            let expected = ['item-id-8u5h2m.json', 'item-id-ko4b27.json', 'item-id-vdpo5l.json', 'item-id-XXXXXXXX.json']
            for (let i = 1; i <= totalSupply; i++) {
                land = await contract._allTokens(i)
                result.push(land[1])
            }
            expect(expected).to.eql(result, 'expected my \'expected\' and my \'result\' are same')

            const tokenId = await contract.tokenOfOwnerByIndex(accounts[2], 2)
            assert.equal(tokenId, 4, 'tokenId is correct')
        })
    })

    describe('transfering', async () => {
        it('transfer a token between accounts', async () => {
            await contract.safeTransfer(accounts[2], accounts[1], 2)

            const balanceOfAccount1 = await contract.balanceOf(accounts[1])
            assert.equal(balanceOfAccount1, 2, 'balance of account 1 is correct')

            const balanceOfAccount2 = await contract.balanceOf(accounts[2])
            assert.equal(balanceOfAccount2, 2, 'balance of account 2 is correct')

        })
    })
})