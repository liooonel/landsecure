const Validator = require('validatorjs');
Validator.useLang('fr')
const { pool, client } = require('../config/db')
/**
 *  Passwords must be 
 * - At least 8 characters long, max length anything
 * - Include at least 1 lowercase letter
 * - 1 capital letter
 * - 1 number
 * - 1 special character => !@#$%^&*
 */
const passwordRegex = /^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[\w!@#$%^&*]{8,}$/;

Validator.register(
    'strict',
    function (value, requirement, attribute) {
        return passwordRegex.test(value);
    },
    'Le champ :attribute doit contenir au moins une lettre majuscule, une lettre minuscule, un chiffre et un caractère spécial.'
    // 'The :attribute must contain at least one uppercase letter, one lowercase letter and one number'
)

Validator.registerAsync('exist', function (value, attribute, req, passes) {
    if (!attribute) throw new Error('Specify Requirements i.e fieldName: exist:table,column');
    //split table and column
    let attArr = attribute.split(",");
    if (attArr.length !== 2) throw new Error(`Invalid format for validation rule on ${attribute}`);

    //assign array index 0 and 1 to table and column respectively
    const { 0: table, 1: column } = attArr;

    //define custom error message
    // let msg = (column == "email") ? `${column} has already been taken` : `${column} already in use`
    //check if incoming value already exists in the database

    pool.query(`SELECT exists (SELECT 1 FROM users WHERE deleted=\'false\' AND ${column} = $1 LIMIT 1)`, [value], (err, res) => {
        if (res.rows[0].exists === true) { // ou juste res.rows[0].exists
            passes(false, `${value} a déjà été pris.`)
            // passes(false, 'Username has already been taken.'); // if value is not available
            return;
        }
        passes(); // if value is available
    })
});

const validator = (body, rules, customMessages, callback) => {
    const validation = new Validator(body, rules, customMessages);
    validation.passes(() => callback(null, true));
    validation.fails(() => callback(validation.errors, false));
};

module.exports = validator;