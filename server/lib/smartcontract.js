require('dotenv/config')
const Web3 = require('web3')
const Land = require('../smartcontract/build/contracts/Land')

const web3 = new Web3(process.env.PROVIDER_STRING)

const contract = function getId() {
    try {
        return web3.eth.net.getId().then(async id => {
            if (id) {
                const deployedNetwork = Land.networks[id]
                const contract = new web3.eth.Contract(Land.abi, deployedNetwork.address)
                return contract
            }
        })
    } catch (error) {
        throw error
    }
}

module.exports = { contract }