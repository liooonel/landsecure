const jwt = require('jsonwebtoken')

const generateTokenForUser = function (userData) {
    return jwt.sign({
        userId: userData.id,
        account: userData.account
    }, process.env.JWT_SIGN_TOKEN, { expiresIn: '10h' })
}

module.exports = generateTokenForUser