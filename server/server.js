const express = require('express')
const app = express()
const cors = require('cors')
const ownerRouter = require('./routes/owner')
const notaryRouter = require('./routes/notary')
const adminRouter = require('./routes/admin')
const { checkTokenOwner, checkTokenAdmin, checkTokenNotary, checkToken } = require('./middlewares/verifyToken')
const { loginUser, storeUser } = require('./controllers/users')
const { storeUserValidation, loginUserValidation } = require('./middlewares/users')
const { showLands } = require('./controllers/lands')

app.use(express.json())
app.use(cors())
app.use('/tokens', express.static('tokens/images'))

app.use('/owner', checkTokenOwner, ownerRouter)
app.use('/notary', checkTokenNotary, notaryRouter)
app.use('/admin', checkTokenAdmin, adminRouter)

app.post('/login', [checkToken, loginUserValidation], loginUser)
app.post('/register', storeUserValidation, storeUser)
app.get('/lands', showLands)


app.listen(3000, () => { console.log('Server started') })