const validator = require('../lib/validate')
const { pool, client } = require('../config/db')

const storeUserValidation = (req, res, next) => {
    const rules = {
        email: 'required_without:phone|email|exist:users,email',
        name: 'required|string',
        password: 'required|string|min:8|confirmed|strict',
        phone: 'required_without:email|digits_between:8,11|exist:users,phone',
        publickey: 'required|string|size:42|exist:users,publickey',
        account: 'required|string|in:notary,owner,admin'
    }

    const customErrorMessages = {}

    validator(req.body, rules, customErrorMessages, (err, status) => {
        if (!status) {
            res.status(412).send(err)
            // .send({
            //     success: false,
            //     message: 'Validation failed',
            //     data: err
            // });
        } else {
            next();
        }
    })
}

const getUser = async (req, res, next) => {
    let user
    try {
        user = await pool.query('SELECT * FROM users WHERE id = $1 AND deleted = false', [req.params.id])
        if (user.rowCount == 0) {
            return res.status(404).json({ message: 'Cannot find user' })
        }
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    res.user = user
    next()
}

const getUserConnected = async (req, res, next) => {
    let user
    try {
        user = await pool.query('SELECT * FROM users WHERE id = $1 AND deleted = false', [req.authData.userId])
        if (user.rowCount == 0) {
            return res.status(404).json({ message: 'Utilisateur introuvable.' }) //Cannot find user
        }
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    res.user = user
    next()
}

const updateUserValidation = (req, res, next) => {
    const rules = {
        email: 'email',
        name: 'string',
        password: 'string|min:8|strict',
        phone: 'digits_between:8,11',
        publickey: 'string|size:42',
        account: 'string|in:notary,owner,admin'
    }

    const customErrorMessages = {}

    validator(req.body, rules, customErrorMessages, (err, status) => {
        if (!status) {
            res.status(412).send(err)
            // .send({
            //     success: false,
            //     message: 'Validation failed',
            //     data: err
            // });
        } else {
            next();
        }
    })
}

const loginUserValidation = (req, res, next) => {
    const rules = {
        email: 'required|email',
        password: 'required|string|min:8|strict',
    }

    const customErrorMessages = {}

    validator(req.body, rules, customErrorMessages, (err, status) => {
        if (!status) {
            res.status(412).send(err)
            // .send({
            //     success: false,
            //     message: 'Validation failed',
            //     data: err
            // });
        } else {
            next();
        }
    })
}

const getUserByAdress = async (req, res, next) => {
    let user
    try {
        user = await pool.query('SELECT * FROM users WHERE publickey = $1 AND deleted = false AND account= \'owner\' ', [req.params.publickey])
        if (user.rowCount == 0) {
            return res.status(404).json({ message: 'Cannot find user' })
        }
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    res.user = user
    next()
}

module.exports = {
    storeUserValidation,
    getUser,
    updateUserValidation,
    loginUserValidation,
    getUserConnected,
    getUserByAdress
}