const jwt = require('jsonwebtoken')

const checkTokenOwner = function (req, res, next) {
    const bearerHeader = req.headers['authorization']
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ')
        const bearToken = bearer[1]
        jwt.verify(bearToken, process.env.JWT_SIGN_TOKEN, (err, authData) => {
            if (err) {
                return res.status(403).json({ message: err.message })
            } else if (authData.account == 'owner') {
                req.authData = authData
                next()
            } else {
                res.status(403).json({ message: 'Access forbidden' })
            }
        })
    } else {
        res.status(403).json({ message: 'Access forbidden' })
    }
}

const checkTokenAdmin = function (req, res, next) {
    const bearerHeader = req.headers['authorization']
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ')
        const bearToken = bearer[1]
        jwt.verify(bearToken, process.env.JWT_SIGN_TOKEN, (err, authData) => {
            if (err) {
                return res.status(403).json({ message: err.message })
            } else if (authData.account == 'admin') {
                req.authData = authData
                next()
            } else {
                res.status(403).json({ message: 'Access forbidden' })
            }
        })
    } else {
        res.status(403).json({ message: 'Access forbidden' })
    }
}

const checkTokenNotary = function (req, res, next) {
    const bearerHeader = req.headers['authorization']
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ')
        const bearToken = bearer[1]
        jwt.verify(bearToken, process.env.JWT_SIGN_TOKEN, (err, authData) => {
            if (err) {
                return res.status(403).json({ message: err.message })
            } else if (authData.account == 'notary') {
                req.authData = authData
                next()
            } else {
                res.status(403).json({ message: 'Access forbidden' })
            }
        })
    } else {
        res.status(403).json({ message: 'Access forbidden' })
    }
}

const checkToken = function (req, res, next) {
    const bearerHeader = req.headers['authorization']
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ')
        const bearToken = bearer[1]
        jwt.verify(bearToken, process.env.JWT_SIGN_TOKEN, (err, authData) => {
            if (err) {
                return res.status(403).json({ message: err.message })
            } else if (authData.account == 'notary' || authData.account == 'admin' || authData.account == 'owner') {
                req.authData = authData
                next()
            } else {
                res.status(403).json({ message: 'Access forbidden' })
            }
        })
    } else {
        next()
    }
}

module.exports = {
    checkTokenOwner, checkTokenAdmin, checkTokenNotary, checkToken
}