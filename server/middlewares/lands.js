const validator = require('../lib/validate')
const { contract } = require('../lib/smartcontract')
const walkSync = require('../lib/fileContent')
const { pool, client } = require('../config/db')
const Web3 = require('web3')
const web3 = new Web3(process.env.PROVIDER_STRING)
const fs = require('fs-extra')
const path = require('path')
var deepEqual = require('deep-equal')

const storeLandValidation = (req, res, next) => {
    const rules = {
        ownerid: "required|integer",
        createdby: "required|integer",
        titrefoncier: "required|string",
        // image: "required|string",
        description: "required|string",
        borne1: {
            lat: "required|numeric",
            lng: "required|numeric"
        },
        borne2: {
            lat: "required|numeric",
            lng: "required|numeric"
        },
        borne3: {
            lat: "required|numeric",
            lng: "required|numeric"
        },
        borne4: {
            lat: "required|numeric",
            lng: "required|numeric"
        },
        borne5: {
            lat: "numeric",
            lng: "numeric"
        },
        borne6: {
            lat: "numeric",
            lng: "numeric"
        }
    }

    const customErrorMessages = {}

    validator(req.body, rules, customErrorMessages, (err, status) => {
        if (!status) {
            res.status(412).send(err)
            // .send({
            //     success: false,
            //     message: 'Validation failed',
            //     data: err
            // });
        } else {
            next();
        }
    })
}

const getLand = async (req, res, next) => {
    let land
    const exist = await (await contract()).methods.tokenExist(req.params.id).call()
    if (exist == false) return res.status(404).json({ message: 'Ressource introuvable.' }) //Cannot find land
    try {
        land = await pool.query('SELECT id, "tokenURI", ST_AsGeoJSON(geom) AS geometry, created_by, created_at, modified_by, modified_at FROM lands WHERE id = $1', [req.params.id])
        // ST_AsText
        if (land.rowCount == 0) {
            return res.status(404).json({ message: 'Ressource introuvable.' }) //Cannot find land
        }
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    res.land = land
    next()
}

const updateLandvalidation = (req, res, next) => {
    const rules = {
        modifiedby: "required|integer",
        titrefoncier: "string",
        description: "string",
        borne1: {
            lat: "numeric",
            lng: "numeric"
        },
        borne2: {
            lat: "numeric",
            lng: "numeric"
        },
        borne3: {
            lat: "numeric",
            lng: "numeric"
        },
        borne4: {
            lat: "numeric",
            lng: "numeric"
        },
        borne5: {
            lat: "numeric",
            lng: "numeric"
        },
        borne6: {
            lat: "numeric",
            lng: "numeric"
        }
    }

    const customErrorMessages = {}

    validator(req.body, rules, customErrorMessages, (err, status) => {
        if (!status) {
            res.status(412).send(err)
            // .send({
            //     success: false,
            //     message: 'Validation failed',
            //     data: err
            // });
        } else {
            next();
        }
    })
}

const landValidation = (req, res, next) => {
    const rules = {
        recipientKey: "required|string|size:42"
    }

    const customErrorMessages = {}

    validator(req.body, rules, customErrorMessages, (err, status) => {
        if (!status) {
            res.status(412).send(err)
            // .send({
            //     success: false,
            //     message: 'Validation failed',
            //     data: err
            // });
        } else {
            next();
        }
    })
}

const ownerOf = async (req, res, next) => {
    await (await contract()).methods.ownerOf(req.params.id).call({}, function (error, result) {
        if (result != res.user.rows[0].publickey) {
            return res.status(401).send({ 'message': 'Vous n\'êtes pas le propriétaire de cette parcelle.' })
        }
        next()
    })
}

const verifyExist = async (req, res, next) => {
    let tokenURI = 'land-gen-' + req.body.titrefoncier.toLowerCase() + '.json'
    let pass = false

    // Check if the file exists in the current directory.
    fs.access(path.join(__dirname, '../tokens/files/' + tokenURI), fs.constants.F_OK, async (err) => {
        // console.log(`${file} ${err ? 'does not exist' : 'exists'}`);
        if (err) { //does not exist

            // sinon j'ouvre tous les fichiers pr verifier les bornes
            //recupérer nbre de fichier chiffre
            const fileList = walkSync(path.join(__dirname, '../tokens/files/'), [])
            // console.log(fileList.length)

            if (fileList.length != 0) {
                for (let i = 0; i < fileList.length; i++) {
                    const element = fileList[i];
                    try {
                        const elementObj = await fs.readJson(element)
                        // console.log(elementObj)
                        if (deepEqual(req.body.borne1, elementObj.borne1) &&
                            deepEqual(req.body.borne2, elementObj.borne2) &&
                            deepEqual(req.body.borne3, elementObj.borne3) &&
                            deepEqual(req.body.borne4, elementObj.borne4) &&
                            deepEqual(req.body.borne5, elementObj.borne5) &&
                            deepEqual(req.body.borne6, elementObj.borne6)) {
                            // console.log("found")
                            return res.status(406).send({ message: 'Vous ne pouvez pas ajouter cette parcelle' })
                        } else {
                            pass = true
                        }
                    } catch (err) {
                        return res.status(500).send(err)
                    }
                }
            } else {
                pass = true
            }

            if (pass == true) {
                next()
            }
        } else { //exists
            return res.status(406).send({ message: 'Vous ne pouvez pas ajouter cette parcelle' })
        }
    });
}

module.exports = { storeLandValidation, getLand, updateLandvalidation, landValidation, ownerOf, verifyExist }