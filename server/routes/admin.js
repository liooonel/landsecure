const express = require('express')
const router = express.Router()
const { indexUser, storeUser, showUser, updateUser, destroyUser, logoutUser } = require('../controllers/users')
const { storeUserValidation, getUser, updateUserValidation } = require('../middlewares/users')

router.get('/user', indexUser)
router.get('/user/:id', getUser, showUser)
router.post('/user/register', storeUserValidation, storeUser) 
router.put('/user/:id', [getUser, updateUserValidation], updateUser)
router.delete('/user/:id', getUser, destroyUser)
// router.post('/logout', logoutUser)

module.exports = router