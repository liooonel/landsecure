const express = require('express')
const router = express.Router()
const { showUser, updateUserOwner, logoutUser, balanceOf } = require('../controllers/users')
const { getUserConnected, updateUserValidation } = require('../middlewares/users')
const { transferLand, historical, showLand } = require('../controllers/lands')
const { getLand, landValidation, ownerOf } = require('../middlewares/lands')

router.get('/show', getUserConnected, showUser)
// router.post('/register', storeUserValidation, storeUser)
router.put('/update', [getUserConnected, updateUserValidation], updateUserOwner)
// router.post('/logout', logoutUser)
router.post('/transfer/:id', [getLand, landValidation], transferLand)
router.get('/historical/:id', [getUserConnected, getLand, ownerOf], historical)
router.get('/balance', getUserConnected, balanceOf)

router.get('/land/:id', [getUserConnected, getLand, ownerOf], showLand)

module.exports = router