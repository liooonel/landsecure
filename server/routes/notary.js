const express = require('express')
const router = express.Router()
const { showUser, logoutUser, balanceOf, listOwner, returnOwner } = require('../controllers/users')
const { getUserConnected, getUser, getUserByAdress } = require('../middlewares/users')
const { indexLand, showLand, storeLand, updateLand, historical, showOwnerOf } = require('../controllers/lands')
const { storeLandValidation, getLand, updateLandvalidation, ownerOf, verifyExist } = require('../middlewares/lands')

router.get('/show', getUserConnected, showUser) //ok
// router.post('/logout', logoutUser)
router.get('/land', indexLand) // ok
router.get('/land/:id', getLand, showLand) //ok
router.post('/land/store', [verifyExist, storeLandValidation], storeLand) //ok
router.put('/land/:id', [verifyExist, getLand, updateLandvalidation], updateLand) //ok
// router.delete('/land/:id', destroyLand)
router.get('/historical/:id', getLand, historical) //ok
router.get('/balanceof/:id', getUser, balanceOf)
router.get('/ownerof/:id', getLand, showOwnerOf)

router.get('/list/owner', listOwner) //ok
router.get('/owner/:publickey', getUserByAdress, returnOwner) //ok

module.exports = router