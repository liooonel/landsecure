const { pool, client } = require('../config/db')
const { contract } = require('../lib/smartcontract')
const fs = require('fs-extra')
const path = require('path')
const Web3 = require('web3')
const web3 = new Web3(process.env.PROVIDER_STRING)
const GeoJSON = require('geojson')

const indexLand = async (req, res) => {
    // visualiser sur une carte toutes les parcelles
    // ce  lien doit préparer le necessaire pour le faire
    // meme si c'est envois d'infos à une vue --NON PAS ICI 
    // ICI ON AFFICHE JUSTE LA LISTE DES PARCELLES DISPONIBLES AVEC LES INFOS NECESSAIRES FAISABLE PAR LES NOTAIRES
    let tokenURItabs = []
    let tokenIdtab = []
    await (await contract()).methods.totalSupply().call({}, async function (error, result) {
        if (error) {
            return res.send({ 'message': error })
        }
        for (let i = 0; i < result; i++) {
            await (await contract()).methods.tokenByIndex(i).call({}, async function (error, tokenId) {
                if (error) {
                    return res.send({ 'message': error })
                }
                tokenIdtab.push(tokenId)
                await (await contract()).methods.tokenURI(tokenId).call({}, async function (error, tokenURI) {
                    if (error) {
                        return res.send({ 'message': error })
                    }
                    tokenURItabs.push(tokenURI)
                    if (i == result - 1) {
                        let content = []
                        for (let i = 0; i < tokenURItabs.length; i++) {
                            try {
                                content.push(await fs.readJson(path.join(__dirname, '../tokens/files/' + tokenURItabs[i])))
                            } catch (error) {
                                res.status(500).json({ message: error.message })
                            }

                        }
                        res.status(200).json({ file: content, jeton: tokenIdtab })
                    }
                })
            })
        }
    })

}

const showLand = async (req, res) => {
    // Afficher les informations sur une parcelle
    // Middleware: utilisateur connecté à son compte et également connexion à son portefeuille requis
    try {
        const content = await fs.readJson(path.join(__dirname, '../tokens/files/' + res.land.rows[0].tokenURI))
        res.status(200).json({ database: res.land.rows, file: content })
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const storeLand = async (req, res) => {
    //Les infos depuis le formulaire sans le ownerId et modifyBy dans data
    const data = JSON.parse(JSON.stringify(req.body))
    delete data.ownerid
    delete data.createdby

    //génération tokenURI
    // let tokenURI = 'land-gen-' + new Date().getTime() + '.json'
    let tokenURI = 'land-gen-' + req.body.titrefoncier.toLowerCase() + '.json'

    //variable adresse client
    let address
    //variable userId client
    let userId

    //récupération des comptes de la blockchain
    addresses = await web3.eth.getAccounts()

    //Début récupération en bdd de l'adresse (clé publique) du propriétaire en fonction de son id
    try {
        user = await pool.query('SELECT * FROM users WHERE id = $1 AND deleted = false AND account = \'owner\'', [req.body.ownerid])
        if (user.rowCount == 0) {
            return res.status(404).json({ message: 'Cannot find owner account' })
        }
        address = user.rows[0].publickey;
        userId = user.rows[0].id
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    //Fin récupération

    // Vérification que created_by est bien un  notaire
    try {
        user = await pool.query('SELECT * FROM users WHERE id = $1 AND deleted = false AND account = \'notary\' ', [req.body.createdby])
        if (user.rowCount == 0) {
            return res.status(404).json({ message: 'Cannot find notary account' })
        }
        createdbyId = user.rows[0].id
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    // Fin vérification

    // Début formatage de nos coordonnées sous format GeoJSON
    const polygon = [
        [
            [Number(data.borne1.lat), Number(data.borne1.lng)],
            [Number(data.borne2.lat), Number(data.borne2.lng)],
            [Number(data.borne3.lat), Number(data.borne3.lng)],
            [Number(data.borne4.lat), Number(data.borne4.lng)]
        ]
    ]
    if (data.borne5.lat != "" && data.borne5.lng != "") polygon[0].push([Number(data.borne5.lat), Number(data.borne5.lng)])
    if (data.borne6.lat != "" && data.borne6.lng != "") polygon[0].push([Number(data.borne6.lat), Number(data.borne6.lng)])
    polygon[0].push([Number(data.borne1.lat), Number(data.borne1.lng)])
    const data2 = { polygon: polygon }

    const landG = GeoJSON.parse(data2, { 'Polygon': 'polygon' })
    //Fin GeoJSON

    //derniere image du dossier image ou image default 
    let myImage
    fs.readdir(path.join(__dirname, '../tokens_waiting/'), (err, items) => {
        if (err) { return res.status(500).send(err) }
        const tokenImageTab = []
        if (items.length != 1) {
            for (let i = 0; i < items.length; i++) {
                if (items[i] != 'default.jpg') {
                    tokenImageTab.push(items[i])
                }
            }
            myImage = tokenImageTab[0]
        } else {
            myImage = 'default.jpg'
        }
        data.image = myImage
    })

    if (address && tokenURI) {
        let gas
        //Estimation du gas nécessaire pour la création de notre propriété
        try {
            gas = await (await contract()).methods.safeMint(address, tokenURI).estimateGas()
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }

        //Création de la propriété dans la blockchain
        (await contract()).methods.safeMint(address, tokenURI).send({ from: addresses[0], gas: gas })
            .on('receipt', async function (receipt) {
                let tokenId
                try {
                    //Sauvegarde de l'image de description de la propriété dans le bon dossier
                    if (myImage == 'default.jpg') {
                        fs.copySync(path.join(__dirname, '../tokens_waiting/' + myImage), path.join(__dirname, '../tokens/images/' + myImage))
                    } else {
                        fs.moveSync(path.join(__dirname, '../tokens_waiting/' + myImage), path.join(__dirname, '../tokens/images/' + myImage), { overwrite: true })
                    }
                    //Création du fichier json relié au tokenURI
                    fs.writeJsonSync(path.join(__dirname, '../tokens/files/' + tokenURI), data, { spaces: 2 })

                    //La récupération se fait de cette maniere parce que dans la fonction de mon smartcontract, qui s'occupe de la création de token
                    //seule un safemint est une fonction interne
                    //donc seul son résultat est attendu
                    tokenId = receipt.events.Transfer.returnValues.tokenId
                } catch (error) {
                    return res.status(500).json({ message: error.message })
                }

                // Sauvegarde de la propriété en base de données
                try {
                    const response = await pool.query('INSERT INTO lands(id, geom, "tokenURI", created_by, created_at) VALUES ($1, ST_AsText(ST_GeomFromGeoJSON($2)), $3, $4, NOW())', [tokenId, landG.geometry, tokenURI, createdbyId])
                    res.status(201).json({ message: 'Land added successfully' })
                } catch (error) {
                    return res.status(500).json({ message: error.message })
                }
                // Fin sauvegarde de la propriété en base de données

                // Sauvegarde pour historique
                try {
                    const response = await pool.query('INSERT INTO history(owner_id, land_id) VALUES ($1, $2)', [userId, tokenId])
                } catch (error) {
                    return res.status(500).json({ message: error.message })
                }
                // Fin sauvegarde pour historique

            })
            .on('error', function (error, receipt) {
                return res.status(500).json({ message: error.message })
            })
    } else {
        res.send({
            'message': 'User address or tokenURI is missing.'
        })
    }
    // revoir mes coordonnées
    // Middleware: utilisateur connecté à son compte et également connexion à son portefeuille ether requis
}

const updateLand = async (req, res) => {
    //Les infos depuis le formulaire sans le modifiedby dans data
    const data = JSON.parse(JSON.stringify(req.body))
    delete data.modifiedby

    let tokenURI = 'land-gen-' + req.body.titrefoncier.toLowerCase() + '.json' //nouveau titre foncier    
    addresses = await web3.eth.getAccounts() //récupération des comptes de la blockchain

    // Vérification que modifiedby est bien un notaire
    try {
        user = await pool.query('SELECT * FROM users WHERE id = $1 AND deleted = false AND account = \'notary\' ', [req.body.modifiedby])
        if (user.rowCount == 0) {
            return res.status(404).json({ message: 'Cannot find notary account' })
        }
        modifiedbyId = user.rows[0].id
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    // Fin vérification

    //Début récupération en bdd de l'adresse (clé publique) du propriétaire en fonction de son id
    try {
        user = await pool.query('SELECT users.id, users.publickey FROM history, users WHERE history.land_id = $1 AND history.owner_id = users.id ORDER BY history.id DESC LIMIT 1', [req.params.id])
        if (user.rowCount == 0) {
            return res.status(404).json({ message: 'Cannot find owner account' })
        }
        address = user.rows[0].publickey;
        userId = user.rows[0].id
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    //Fin récupération

    // Début formatage de nos coordonnées sous format GeoJSON
    const polygon = [
        [
            [Number(data.borne1.lat), Number(data.borne1.lng)],
            [Number(data.borne2.lat), Number(data.borne2.lng)],
            [Number(data.borne3.lat), Number(data.borne3.lng)],
            [Number(data.borne4.lat), Number(data.borne4.lng)]
        ]
    ]
    if (data.borne5.lat != "" && data.borne5.lng != "") polygon[0].push([Number(data.borne5.lat), Number(data.borne5.lng)])
    if (data.borne6.lat != "" && data.borne6.lng != "") polygon[0].push([Number(data.borne6.lat), Number(data.borne6.lng)])
    polygon[0].push([Number(data.borne1.lat), Number(data.borne1.lng)])
    const data2 = { polygon: polygon }

    const landG = GeoJSON.parse(data2, { 'Polygon': 'polygon' })
    //Fin GeoJSON

    //Garder la même image 
    try {
        const elementObj = await fs.readJson(path.join(__dirname, '../tokens/files/' + res.land.rows[0].tokenURI))
        data.image = elementObj.image
    } catch (err) {
        return res.status(500).send(err)
    }
    //Fin récupération

    /**DEMARRAGE */
    if (address && tokenURI) {
        //reset in blockchain
        let gas
        try {
            gas = await (await contract()).methods.resetToken(req.params.id, address, tokenURI).estimateGas()
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }

        (await contract()).methods.resetToken(req.params.id, address, tokenURI).send({ from: addresses[0], gas: gas })
            .on('receipt', async function (receipt) {
                let token

                try {
                    //suppression du fichier .json dans le dossier avec nom de la base de données
                    fs.unlinkSync(path.join(__dirname, '../tokens/files/' + res.land.rows[0].tokenURI));
                    //Création du fichier json relié au tokenURI basé sur le nouveau TF
                    fs.writeJsonSync(path.join(__dirname, '../tokens/files/' + tokenURI), data, { spaces: 2 })

                    //La récupération se fait de cette maniere parce que dans la fonction de mon smartcontract qui s'occupe du reset contient deux
                    //fonctions internes à savoir le burn et le mint alors nous avons deux result attendu celui du burn et celui du mint. je m'interesse ici
                    //à celui du mint car c'est cette fonction qui est exécuté en second
                    // console.log(receipt.events.Transfer)
                    token = receipt.events.Transfer[1].returnValues.tokenId
                } catch (error) {
                    return res.status(500).json({ message: error.message })
                }

                // // Modification de la propriété en base de données
                try {
                    const response = await pool.query('UPDATE lands SET id=$1, "tokenURI"=$2, geom=ST_AsText(ST_GeomFromGeoJSON($3)), modified_by=$4, modified_at=NOW() WHERE id=$5', [token, tokenURI, landG.geometry, modifiedbyId, req.params.id])
                    res.status(201).json({ message: 'Land modified successfully' })
                } catch (error) {
                    return res.status(500).json({ message: error.message })
                }
                // Fin modification de la propriété en base de données

            })
            .on('error', function (error, receipt) {
                return res.status(500).json({ message: error.message })
            })
    } else {
        res.send({
            'message': 'User address or tokenURI is missing.'
        })
    }

}

const transferLand = async (req, res) => {
    //Début récupération en bdd de l'adresse (clé publique) de l'expéditeur en fonction de son id
    try {
        user = await pool.query('SELECT * FROM users WHERE id = $1 AND deleted = false AND account = \'owner\'', [req.authData.userId])
        if (user.rowCount == 0) {
            return res.status(404).json({ message: 'Cannot find owner account' })
        }
        ownerAddress = user.rows[0].publickey;
        ownerId = user.rows[0].id
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    //Fin récupération

    //Début récupération en bdd de l'adresse (clé publique) du récepteur en fonction de son id
    try {
        user = await pool.query('SELECT * FROM users WHERE publickey = $1 AND deleted = false AND account = \'owner\'', [req.body.recipientKey])
        if (user.rowCount == 0) {
            return res.status(404).json({ message: 'Cannot find owner account' })
        }
        recipientAddress = user.rows[0].publickey;
        recipientId = user.rows[0].id
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    //Fin récupération

    if (ownerAddress && recipientAddress) {
        let gas
        //Estimation du gas
        try {
            gas = await (await contract()).methods.safeTransfer(ownerAddress, recipientAddress, res.land.rows[0].id).estimateGas()
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }

        //Création de la propriété dans la blockchain
        (await contract()).methods.safeTransfer(ownerAddress, recipientAddress, res.land.rows[0].id).send({ from: ownerAddress, gas: gas })
            .on('receipt', async function (receipt) {
                // Sauvegarde pour historique
                try {
                    const response = await pool.query('INSERT INTO history(owner_id, land_id) VALUES ($1, $2)', [recipientId, res.land.rows[0].id])
                    res.status(201).json({ message: 'Successful transfer' })
                } catch (error) {
                    return res.status(500).json({ message: error.message })
                }
                // Fin sauvegarde pour historique
            })
            .on('error', function (error, receipt) {
                return res.status(500).json({ message: error.message })
            })
    } else {
        res.send({
            'message': 'Addresses are missing.'
        })
    }
    //penser s'il faut que le notaire puisse effectuer aussi des transferts de parcelles
}

const historical = async (req, res) => {
    // const userId = res.user.rows[0].id
    try {
        historique = await pool.query('SELECT * FROM history, users WHERE history.owner_id = users.id AND land_id = $1 AND users.deleted = false ORDER BY history.id DESC ', [req.params.id])
        if (historique.rowCount == 0) {
            return res.status(404).json({ message: 'Aucune information disponible.' }) //No entries found
        }
        return res.status(200).json(historique.rows)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

const showOwnerOf = async (req, res) => {
    await (await contract()).methods.ownerOf(req.params.id).call({}, async function (error, result) {
        if (error) {
            return res.send({ 'message': error })
        }
        try {
            user = await pool.query('SELECT * FROM users WHERE publickey = $1 AND deleted = false', [result])
            if (user.rowCount == 0) {
                return res.status(404).json({ message: 'Cannot find user' })
            }
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }
        return res.status(200).json({ owner: user.rows })
    })
}

const showLands = async (req, res) => {
    let tokenIdTabs = []
    const totalSupply = await (await contract()).methods.totalSupply().call()
    for (let i = 0; i < totalSupply; i++) {
        await (await contract()).methods.tokenByIndex(i).call({}, async function (error, tokenId) {
            if (error) {
                return res.status(500).send({ 'message': error })
            }
            tokenIdTabs.push(tokenId)
            if (i == totalSupply - 1) {
                let content = []
                for (let i = 0; i < tokenIdTabs.length; i++) {
                    try {
                        const response = await pool.query('SELECT id, "tokenURI", ST_AsGeoJSON(geom) AS geometry FROM lands WHERE id = $1 ORDER BY id DESC', [tokenIdTabs[i]])
                        // ST_AsText
                        content.push(response.rows)
                    } catch (error) {
                        res.status(500).json({ message: error.message })
                    }
                }
                res.status(200).json({ file: content })
            }
        })
    }
}

module.exports = { indexLand, showLand, storeLand, updateLand, transferLand, historical, showOwnerOf, showLands }