
const { pool, client } = require('../config/db')
const bcrypt = require('bcrypt');
const saltRounds = 10;
const generateTokenForUser = require('../lib/jwt')
const { contract } = require('../lib/smartcontract')
const fs = require('fs-extra')
const path = require('path')

const indexUser = async (req, res) => {
    try {
        const response = await pool.query('SELECT * FROM users WHERE deleted = false')
        res.status(200).json(response.rows)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const showUser = (req, res) => {
    try {
        res.status(200).json(res.user.rows)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const storeUser = async (req, res) => {
    const { email, name, password, phone, publickey, account } = req.body;

    bcrypt.hash(password, saltRounds, async (err, hash) => {
        try {
            const response = await pool.query('INSERT INTO users(email, name, password, phone, publickey, account) VALUES ($1, $2, $3, $4, $5, $6)', [email, name, hash, phone, publickey, account])
            res.status(201).json({
                message: 'User added successfully',
                body: {
                    user: { email, name, hash, phone, publickey, account }
                }
            })
        } catch (error) {
            res.status(500).json({ message: error.message })
        }
    });
}

const updateUser = async (req, res) => {
    let email
    let name
    let password
    let phone
    let publickey
    let account

    try {
        const id = res.user.rows[0].id

        if (req.body.email == null) {
            email = res.user.rows[0].email
        } else {
            email = req.body.email
        }

        if (req.body.name == null) {
            name = res.user.rows[0].name
        } else {
            name = req.body.name
        }

        if (req.body.phone == null) {
            phone = res.user.rows[0].phone
        } else {
            phone = req.body.phone
        }

        if (req.body.publickey == null) {
            publickey = res.user.rows[0].publickey
        } else {
            publickey = req.body.publickey
        }

        if (req.body.account == null) {
            account = res.user.rows[0].account
        } else {
            account = req.body.account
        }

        if (req.body.password == null) {
            password = res.user.rows[0].password
            const response = await pool.query('UPDATE users SET email = $1, name = $2, phone = $3, publickey = $4, account = $5, password = $6 WHERE id = $7', [email, name, phone, publickey, account, password, id])
            res.status(200).json({ message: 'User modified successfully' })
        } else {
            bcrypt.hash(req.body.password, saltRounds, async (err, hash) => {
                try {
                    const response = await pool.query('UPDATE users SET email = $1, name = $2, phone = $3, publickey = $4, account = $5, password = $6 WHERE id = $7', [email, name, phone, publickey, account, hash, id])
                    res.status(200).json({ message: 'User modified successfully' })
                } catch (error) {
                    res.status(500).json({ message: error.message })
                }
            });
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const destroyUser = async (req, res) => {
    try {
        const id = res.user.rows[0].id
        const response = await pool.query('UPDATE users SET deleted = true WHERE id = $1', [id])
        res.status(200).json({ message: 'User deleted successfully' })
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const loginUser = async (req, res) => {
    if (typeof req.authData !== 'undefined') {
        res.json({ message: 'You are already logged in' })
    } else {
        const { email, password } = req.body;
        user = await pool.query('SELECT * FROM users WHERE email = $1 AND deleted = false', [email])
        try {
            if (user.rowCount == 0) {
                return res.status(404).json({ message: 'Impossible de retrouver le compte utilisateur' })
            }
            bcrypt.compare(password, user.rows[0].password, function (err, result) {
                if (result) {
                    const token = generateTokenForUser(user.rows[0])
                    //ajout du token à l'entête ???
                    return res.status(200).json({
                        'user': user.rows[0],
                        'token': token
                    })
                } else {
                    return res.status(403).json({ message: 'Mot de passe invalide' })
                }
            })
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

const logoutUser = (req, res) => { } // je dois penser à la stratégie

const updateUserOwner = async (req, res) => {
    let email
    let name
    let password
    let phone

    try {
        const id = res.user.rows[0].id

        if (req.body.email == null) {
            email = res.user.rows[0].email
        } else {
            email = req.body.email
        }

        if (req.body.name == null) {
            name = res.user.rows[0].name
        } else {
            name = req.body.name
        }

        if (req.body.phone == null) {
            phone = res.user.rows[0].phone
        } else {
            phone = req.body.phone
        }

        if (req.body.password == "") {
            password = res.user.rows[0].password
            const response = await pool.query('UPDATE users SET email = $1, name = $2, phone = $3, password = $4 WHERE id = $5', [email, name, phone, password, id])
            res.status(200).json({ message: 'User modified successfully' })
        } else {
            bcrypt.hash(req.body.password, saltRounds, async (err, hash) => {
                try {
                    const response = await pool.query('UPDATE users SET email = $1, name = $2, phone = $3, password = $4 WHERE id = $5', [email, name, phone, hash, id])

                    res.status(200).json({ message: 'User modified successfully' })
                } catch (error) {
                    res.status(500).json({ message: error.message })
                }
            });
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const balanceOf = async (req, res) => {
    const ownerAddress = res.user.rows[0].publickey
    let tokenURItabs = []
    let tokenIdtab = []
    if (ownerAddress) {
        await (await contract()).methods.balanceOf(ownerAddress).call({}, async function (error, result) {
            if (error) {
                return res.send({ 'message': error })
            }
            for (let i = 0; i < result; i++) {
                await (await contract()).methods.tokenOfOwnerByIndex(ownerAddress, i).call({}, async function (error, tokenId) {
                    if (error) {
                        return res.send({ 'message': error })
                    }
                    tokenIdtab.push(tokenId)
                    await (await contract()).methods.tokenURI(tokenId).call({}, async function (error, tokenURI) {
                        if (error) {
                            return res.send({ 'message': error })
                        }
                        tokenURItabs.push(tokenURI)
                        if (i == result - 1) {
                            let content = []
                            for (let i = 0; i < tokenURItabs.length; i++) {
                                try {
                                    content.push(await fs.readJson(path.join(__dirname, '../tokens/files/' + tokenURItabs[i])))
                                } catch (error) {
                                    res.status(500).json({ message: error.message })
                                }

                            }
                            res.status(200).json({ file: content, jeton: tokenIdtab })
                        }
                    })
                })
            }
        })

    } else {
        res.send({ 'message': 'Owner address are missing.' })
    }
}

const listOwner = async (req, res) => {
    try {
        const response = await pool.query('SELECT * FROM users WHERE deleted = false AND account=\'owner\'')
        res.status(200).json(response.rows)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const returnOwner = async (req, res) => {
    try {
        res.status(200).json(res.user.rows[0])
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

module.exports = {
    indexUser,
    storeUser,
    showUser,
    updateUser,
    destroyUser,
    loginUser,
    logoutUser,
    updateUserOwner,
    balanceOf,
    listOwner,
    returnOwner
}