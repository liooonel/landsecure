const { Pool, Client } = require('pg')
require('dotenv/config')

const pool = new Pool({
    connectionString: process.env.CONNECTION_STRING,
})

const client = new Client({
    connectionString: process.env.CONNECTION_STRING,
})
client.connect()

module.exports = { pool, client }