import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import createPersistedState from "vuex-persistedstate";
// import SecureLS from "secure-ls";
// const ls = new SecureLS({ isCompression: false });

Vue.use(Vuex)

export default new Vuex.Store({
    strict: true,
    state: {
        token: null,
        user: null,
        isUserLoggedIn: false,
        balanceAcc: null,
        notary: null
    },
    plugins: [createPersistedState({ storage: window.sessionStorage })],
    // plugins: [
    //     createPersistedState({
    //         storage: {
    //             getItem: key => ls.get(key),
    //             setItem: (key, value) => ls.set(key, value),
    //             removeItem: key => ls.remove(key)
    //         }
    //     })
    // ],
    mutations: {
        setToken(state, token) {
            state.token = token
            if (token != null) {
                localStorage.setItem('user-token', token)
                axios.defaults.headers.common['Authorization'] = token
                state.isUserLoggedIn = true
            } else {
                localStorage.removeItem('user-token')
                delete axios.defaults.headers.common['Authorization']
                state.isUserLoggedIn = false
            }
        },
        setUser(state, user) {
            state.user = user
        },
        setBalanceAcc(state, balance) {
            state.balanceAcc = balance
        },
        setTokenN(state, token) {
            state.token = token
            if (token != null) {
                localStorage.setItem('notary-token', token)
                axios.defaults.headers.common['Authorization'] = token
                state.isUserLoggedIn = true
            } else {
                localStorage.removeItem('notary-token')
                delete axios.defaults.headers.common['Authorization']
                state.isUserLoggedIn = false
            }
        },
        setNotary(state, notary) {
            state.notary = notary
        }
    },
    getters: {
        getBalanceAcc: (state) => state.balanceAcc, //Object.assign(),
        getToken: (state) => state.token,
        getUser: (state) => state.user,
        getNotary: (state) => state.notary
    },
    actions: {
        setToken({ commit }, token) {
            commit('setToken', token)
        },
        setUser({ commit }, user) {
            commit('setUser', user)
        },
        setBalanceAcc({ commit }, balance) {
            commit('setBalanceAcc', balance)
        },
        setTokenN({ commit }, token) {
            commit('setTokenN', token)
        },
        setNotary({ commit }, notary) {
            commit('setNotary', notary)
        }
    }
}) 