// import api from '@/services/Api' 
require('dotenv').config()
const superagent = require("superagent")

const baseUrl = process.env.VUE_APP_SERVER_BASE_URL //'http://localhost:3000'

export default {
    get() {
        return superagent
            .get(baseUrl + "/notary/show")
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send()
    },
    storeLand(data) {
        return superagent
            .post(baseUrl + "/notary/land/store")
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send(data)
    },
    getOwners() {
        return superagent
            .get(baseUrl + "/notary/list/owner")
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send()
    },
    getOwner(publickey) {
        return superagent
            .get(baseUrl + "/notary/owner/" + publickey)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send()
    },
    getTokens() {
        return superagent
            .get(baseUrl + "/notary/land")
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send()
    },
    getLandHistorical(id) {
        return superagent
            .get(baseUrl + "/notary/historical/" + id)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send()
    },
    showLocation(id) {
        return superagent
            .get(baseUrl + "/notary/land/" + id)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send()
    },
    updateLand(id, data) {
        return superagent
            .put(baseUrl + "/notary/land/" + id)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send(data)
    },
    getLands() {
        return superagent
            .get(baseUrl + "/lands")
            .send()
    }
}