import axios from 'axios'

export default () => {
    return axios.create({
        baseURL: process.env.VUE_APP_SERVER_BASE_URL //'http://localhost:3000'
    })
}