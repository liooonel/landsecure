// import api from '@/services/Api'
require('dotenv').config()
const superagent = require("superagent")

const baseUrl = process.env.VUE_APP_SERVER_BASE_URL

export default {
    register(credentials) {
        return superagent
            .post(baseUrl + "/register")
            .send(credentials)
    },
    login(credentials) {
        return superagent
            .post(baseUrl + "/login")
            .send(credentials)
    },
    update(credentials) {
        return superagent
            .put(baseUrl + "/owner/update")
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('user-token')
            })
            .send(credentials)
    },
    get() {
        return superagent
            .get(baseUrl + "/owner/show")
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('user-token')
            })
            .send()
    },
    balanceOf() {
        return superagent
            .get(baseUrl + "/owner/balance")
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('user-token')
            })
            .send()
    },
    historical(id) {
        return superagent
            .get(baseUrl + "/owner/historical/" + id)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('user-token')
            })
            .send()
    },
    transferTo(id, recipient) {
        return superagent
            .post(baseUrl + "/owner/transfer/" + id)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('user-token')
            })
            .send(recipient)
    },
    showLocation(id) {
        return superagent
            .get(baseUrl + "/owner/land/" + id)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('user-token')
            })
            .send()
    }
}