// import api from '@/services/Api' 
require('dotenv').config()
const superagent = require("superagent")

const baseUrl = process.env.VUE_APP_SERVER_BASE_URL //'http://localhost:3000'

export default {
    getUsers() {
        return superagent
            .get(baseUrl + "/admin/user")
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send()
    },
    getUser(id) {
        return superagent
            .get(baseUrl + "/admin/user/" + id)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send()
    },
    storeUser(data) {
        return superagent
            .post(baseUrl + "/admin/user/register")
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send(data)
    },
    updateUser(id, data) {
        return superagent
            .put(baseUrl + "/admin/user/" + id)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send(data)
    },
    deleteUser(id) {
        return superagent
            .delete(baseUrl + "/admin/user/" + id)
            .set({
                'Authorization': 'Bearer ' + localStorage.getItem('notary-token')
            })
            .send()
    }
}