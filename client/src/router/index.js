import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import OwnerRegister from '../views/OwnerRegister.vue'
import OwnerPanel from '../views/OwnerPanel.vue'
import OwnerUpdate from '../views/OwnerUpdate.vue'
import OwnerBalance from '../views/OwnerBalance.vue'
import OwnerLandHistorical from '../views/OwnerLandHistorical.vue'
import OwnerTransferTo from '../views/OwnerTransferTo.vue'
import OwnerShowLocation from '../views/OwnerShowLocation.vue'
import NotaryPanel from '../views/NotaryPanel.vue'
import NotaryStoreLand from '../views/NotaryStoreLand.vue'
import NotaryLandBalance from '../views/NotaryLandBalance.vue'
import NotaryLandHistorical from '../views/NotaryLandHistorical.vue'
import NotaryLandInMap from '../views/NotaryLandInMap.vue'
import NotaryUpdateLand from '../views/NotaryUpdateLand.vue'
import LandsInMapForAll from '../views/LandsInMapForAll.vue'
import AdminPanel from '../views/AdminPanel.vue'
import AdminUsersList from '../views/AdminUsersList.vue'
import AdminCreateUser from '../views/AdminCreateUser.vue'
import AdminUpdateUser from '../views/AdminUpdateUser.vue'

import Login from '../views/Login.vue'
import store from '@/store/store'
require('dotenv').config()
const jwt = require('jsonwebtoken')

Vue.use(VueRouter)

const ifNotAuthentificated = (to, from, next) => {
  if (store.state.isUserLoggedIn === true) {
    jwt.verify(store.state.token, process.env.VUE_APP_JWT_SK, (err, decoded) => {
      if (decoded.account == 'notary') {
        next('/notary')
      }
      else if (decoded.account == 'admin') {
        next('/admin')
      }
      else if (decoded.account == 'owner') {
        next('/owner')
      } else {
        next()
      }
    })
    return
  }
  next()

}

const ifOwner = (to, from, next) => {
  if (store.state.isUserLoggedIn === true) {
    jwt.verify(store.state.token, process.env.VUE_APP_JWT_SK, (err, decoded) => {
      if (decoded.account == 'owner') {
        next()
        return
      }
    })
    return
  }
  next('/login')
}

const ifNotary = (to, from, next) => {
  if (store.state.isUserLoggedIn === true) {
    jwt.verify(store.state.token, process.env.VUE_APP_JWT_SK, (err, decoded) => {
      if (decoded.account == 'notary') {
        next()
        return
      }
    })
    return
  }
  next('/login')
}

const ifAdmin = (to, from, next) => {
  if (store.state.isUserLoggedIn === true) {
    jwt.verify(store.state.token, process.env.VUE_APP_JWT_SK, (err, decoded) => {
      if (decoded.account == 'admin') {
        next()
        return
      }
    })
    return
  }
  next('/login')
}

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/register',
    name: 'OwnerRegister',
    component: OwnerRegister
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    beforeEnter: ifNotAuthentificated
  },
  {
    path: '/owner',
    name: 'OwnerPanel',
    component: OwnerPanel,
    beforeEnter: ifOwner
  },
  {
    path: '/owner/update',
    name: 'OwnerUpdate',
    component: OwnerUpdate,
    beforeEnter: ifOwner
  },
  {
    path: '/owner/balance',
    name: 'OwnerBalance',
    component: OwnerBalance,
    beforeEnter: ifOwner
  },
  {
    path: '/owner/historical/:jetonId',
    name: 'OwnerLandHistorical',
    component: OwnerLandHistorical,
    beforeEnter: ifOwner
  },
  {
    path: '/owner/transfer/:jetonId',
    name: 'OwnerTransferTo',
    component: OwnerTransferTo,
    beforeEnter: ifOwner
  },
  {
    path: '/owner/show/:jetonId',
    name: 'OwnerShowLocation',
    component: OwnerShowLocation,
    beforeEnter: ifOwner
  },
  {
    path: '/notary',
    name: 'NotaryPanel',
    component: NotaryPanel,
    beforeEnter: ifNotary
  },
  {
    path: '/notary/land/store',
    name: 'NotaryStoreLand',
    component: NotaryStoreLand,
    beforeEnter: ifNotary
  },
  {
    path: '/notary/land/balance',
    name: 'NotaryLandBalance',
    component: NotaryLandBalance,
    beforeEnter: ifNotary
  },
  {
    path: '/notary/land/historical/:jetonId',
    name: 'NotaryLandHistorical',
    component: NotaryLandHistorical,
    beforeEnter: ifNotary
  },
  {
    path: '/notary/land/show/:jetonId',
    name: 'NotaryLandInMap',
    component: NotaryLandInMap,
    beforeEnter: ifNotary
  },
  {
    path: '/notary/land/update/:jetonId',
    name: 'NotaryUpdateLand',
    component: NotaryUpdateLand,
    beforeEnter: ifNotary
  },
  {
    path: '/lands',
    name: 'LandsInMapForAll',
    component: LandsInMapForAll
  },
  {
    path: '/admin',
    name: 'AdminPanel',
    component: AdminPanel,
    beforeEnter: ifAdmin
  },
  {
    path: '/admin/users',
    name: 'UsersList',
    component: AdminUsersList,
    beforeEnter: ifAdmin
  },
  {
    path: '/admin/create/user',
    name: 'AdminCreateUser',
    component: AdminCreateUser,
    beforeEnter: ifAdmin
  },
  {
    path: '/admin/update/user/:id',
    name: 'AdminUpdateUser',
    component: AdminUpdateUser,
    beforeEnter: ifAdmin
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
