import Vue from 'vue'
import App from './App.vue'
import "./assets/css/style.css"
import router from './router'
import { sync } from 'vuex-router-sync'
import store from '@/store/store'
import axios from 'axios'
import Notifications from 'vue-notification'
import 'leaflet/dist/leaflet.css';

Vue.config.productionTip = false

const token = localStorage.getItem('user-token')
if (token) {
  axios.defaults.headers.common['Authorization'] = token
}

sync(store, router)

Vue.use(Notifications)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
